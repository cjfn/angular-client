import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './views/app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public title: string
  public description: string

  constructor(){
  	this.title ='App de jose Florian';
  	this.description = 'Aplicacion de inicio en angular 2 componente 1'
  }
   ngOnInit() {
  }
}
