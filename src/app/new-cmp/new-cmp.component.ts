import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-cmp',
  templateUrl: '../views/new-cmp.component.html',
  styleUrls: ['./new-cmp.component.css']
})
export class NewCmpComponent implements OnInit {

public title: string;
public objetos: Array<string>;
public ObjetosVisibles: boolean;
public color: string;




  constructor() {
  	this.title = 'Titulo de componente 2',
  	this.objetos = ['objeto1','objeto2','objeto3'];
  	this.ObjetosVisibles = false;

   }

   public showObjetos(){
   		this.ObjetosVisibles=true;
   }

   public hideObjetos(){
   		this.ObjetosVisibles = false;
   }

   changeColor(){
   		console.log(this.color);
   }

  ngOnInit() {
  }

}
